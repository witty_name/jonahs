//
//  GameScene.swift
//  Game
//
//  Created by iD Student on 7/9/15.
//  Copyright (c) 2015 iD Tech. All rights reserved.
//


import SpriteKit

struct BodyType {
    static let None: UInt32 = 0
    static let Enemy: UInt32 = 1
    static let Bullet: UInt32 = 2
    static let Hero: UInt32 = 4
    static let RedZone: UInt32 = 4
}

class GameScene: SKScene, SKPhysicsContactDelegate {

    let heroSpeed: CGFloat = 100.0
    
    let hero = SKSpriteNode(imageNamed: "PlayerShip")
    
    var Score = 0
    
    var scoreLabel = SKLabelNode(fontNamed: "Arial")
    
    var enemySpeed = 2.0
   /* func addSpeed() {
        enemySpeed++
    }
    
    init(timeInterval: 4,
        target: self,
        selector: "test",
        userInfo: nill,
        repeats: true)*/
    
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent){
        
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
    println(size.height)
        
        let bullet = SKSpriteNode(imageNamed: "Bullet")
        
        bullet.size = CGSize(width: 50, height: 50)
        bullet.position = CGPointMake(hero.position.x, hero.position.y)
        
        bullet.physicsBody = SKPhysicsBody(circleOfRadius: bullet.size.width/2)
        bullet.physicsBody?.dynamic = true
        bullet.physicsBody?.categoryBitMask = BodyType.Bullet
        bullet.physicsBody?.contactTestBitMask = BodyType.Enemy
        bullet.physicsBody?.collisionBitMask = 0
        bullet.physicsBody?.usesPreciseCollisionDetection = true
        
        addChild(bullet)
        
        let touch = touches.first as! UITouch
        let touchLocation = touch.locationInNode(self)
        
        
        let vector = CGVectorMake(-(hero.position.x-touchLocation.x), -(hero.position.y-touchLocation.y))
        
        let projectileAction = SKAction.sequence([
            SKAction.repeatAction(
                SKAction.moveBy(vector, duration: 0.5), count: 100),
            SKAction.waitForDuration(50),
            SKAction.removeFromParent()
            ])
        
        
        bullet.runAction(projectileAction)
        }
 
    
    override func didMoveToView(view: SKView) {
        println(size.width)
        let swipeUp: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: Selector("swipedUp:"))
        swipeUp.direction = .Up
        view.addGestureRecognizer(swipeUp)
        
        let swipeDown: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: Selector("swipedDown:"))
        swipeDown.direction = .Down
        view.addGestureRecognizer(swipeDown)
        
       /* let swipeLeft: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: Selector("swipedLeft:"))
        swipeLeft.direction = .Left
        view.addGestureRecognizer(swipeLeft)
        
        let swipeRight: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: Selector("swipedRight:"))
        swipeRight.direction = .Right
        view.addGestureRecognizer(swipeRight)*/
        
        
        backgroundColor = SKColor.blackColor()
        
        var xCoord = size.width * 0.5
        var yCoord = size.height * 0.5
        
        hero.size.height = 50
        hero.size.width = 50

        hero.position = CGPoint(x: 40, y: yCoord)
        
        hero.physicsBody = SKPhysicsBody(rectangleOfSize: hero.size)
        hero.physicsBody?.dynamic = true
        hero.physicsBody?.categoryBitMask = BodyType.Hero
        hero.physicsBody?.contactTestBitMask = BodyType.Enemy
        hero.physicsBody?.collisionBitMask = 0
        
        addChild(hero)
        
        
        scoreLabel.fontColor = UIColor.blueColor()
        scoreLabel.fontSize = 40
        scoreLabel.position = CGPointMake(self.size.width/2, self.size.height-50)
        
        addChild(scoreLabel)
        
        let redZone = SKSpriteNode(color: UIColor.redColor(), size: CGSize(width: 100, height: 400))
        redZone.position = CGPointMake(25, 170)
        
        redZone.physicsBody = SKPhysicsBody(rectangleOfSize: redZone.size)
        redZone.physicsBody?.dynamic = true
        redZone.physicsBody?.categoryBitMask = BodyType.Hero
        redZone.physicsBody?.contactTestBitMask = BodyType.Enemy
        redZone.physicsBody?.collisionBitMask = 0
        redZone.physicsBody?.usesPreciseCollisionDetection = true

        addChild(redZone)
        
        runAction(SKAction.repeatActionForever(SKAction.sequence([SKAction.runBlock(addEnemy), SKAction.waitForDuration(1.0)])))
    
        
        physicsWorld.gravity = CGVectorMake(0,0)
        
        physicsWorld.contactDelegate = self
        
        
    }
    
    func addEnemy() {
        var theEnemy:SKSpriteNode = SKSpriteNode(imageNamed: "Enemies")

        theEnemy.size.height = 50
        theEnemy.size.width = 50
        
        let randomY = random() * ((size.height - theEnemy.size.height/2)-theEnemy.size.height/2) + theEnemy.size.height/2
        
        theEnemy.position = CGPoint(x: size.width + theEnemy.size.width/2, y: randomY)
        
        
        theEnemy.physicsBody = SKPhysicsBody(rectangleOfSize: theEnemy.size)
        theEnemy.physicsBody?.dynamic = true
        theEnemy.physicsBody?.categoryBitMask = BodyType.Enemy
        theEnemy.physicsBody?.contactTestBitMask = BodyType.Bullet
        theEnemy.physicsBody?.collisionBitMask = 0
        
        addChild(theEnemy)
        
        var moveEnemy: SKAction
        
        moveEnemy = SKAction.moveTo(CGPoint(x: -theEnemy.size.width/2, y: randomY), duration: NSTimeInterval(enemySpeed))
        
        theEnemy.runAction(SKAction.sequence([moveEnemy, SKAction.removeFromParent()]))
        enemySpeed = enemySpeed - 0.1
    }
    
    func random() -> CGFloat {
        
        return CGFloat(Float(arc4random()) / Float(UINT32_MAX))
        
    }
   
    /*override func update(currentTime: CFTimeInterval) {
        //Called before each frame is rendered
        //var passedInEnemy:SKSpriteNode = theEnemy as SKSpriteNode
        screenEnd(theEnemy)
    }*/
    
   func swipedUp (sender:UISwipeGestureRecognizer){
        var actionMove: SKAction
        
        if (hero.position.y + heroSpeed >= size.height){
            actionMove = SKAction.moveTo(CGPoint(x: hero.position.x, y: size.height - hero.size.height/2), duration: NSTimeInterval(0.5))
        }
        else {
            actionMove = SKAction.moveTo(CGPoint(x: hero.position.x, y: hero.position.y + heroSpeed), duration: NSTimeInterval(0.5))
        }
        hero.runAction(actionMove)
    }
    
    
    func swipedDown (sender:UISwipeGestureRecognizer){
        var actionMove: SKAction
        
        if (hero.position.y - heroSpeed <= 0){
            actionMove = SKAction.moveTo(CGPoint(x: hero.position.x, y: hero.size.height/2), duration: NSTimeInterval(0.5))
        }
        else {
            actionMove = SKAction.moveTo(CGPoint(x: hero.position.x, y: hero.position.y - heroSpeed), duration: NSTimeInterval(0.5))
        }
        
        hero.runAction(actionMove)
    }
    
    /*func swipedLeft (sender:UISwipeGestureRecognizer){
       
        var actionMove: SKAction
        
        
        if (hero.position.x - heroSpeed <= 0){
            actionMove = SKAction.moveTo(CGPoint(x: hero.size.width/2, y: hero.position.y), duration: NSTimeInterval(0.5))
        }
        else {
            actionMove = SKAction.moveTo(CGPoint(x: hero.position.x - heroSpeed, y: hero.position.y), duration: NSTimeInterval(0.5))
        }
        
        hero.runAction(actionMove)
    }
    
    func swipedRight (sender:UISwipeGestureRecognizer){
        
        var actionMove: SKAction
        
        
        if (hero.position.x + heroSpeed >= size.width){
            actionMove = SKAction.moveTo(CGPoint(x: size.width - hero.size.width/2, y: hero.position.y), duration: NSTimeInterval(0.5))
        }
        else {
            actionMove = SKAction.moveTo(CGPoint(x: hero.position.x + heroSpeed, y: hero.position.y), duration: NSTimeInterval(0.5))
        }
        
        hero.runAction(actionMove)
    }*/
    
    
    func didBeginContact(contact: SKPhysicsContact) {
        let bodyA = contact.bodyA
        let bodyB = contact.bodyB
        
        let contactA = bodyA.categoryBitMask
        let contactB = bodyB.categoryBitMask
        
        if (contactA == BodyType.Enemy) {
            
            if (contactB == BodyType.Bullet) {
                
                bulletHitEnemy(contact.bodyB.node as! SKSpriteNode, enemy: contact.bodyA.node as! SKSpriteNode)
                
            }
                
            else {
                
                heroHitEnemy(contact.bodyB.node as! SKSpriteNode, enemy: contact.bodyA.node as! SKSpriteNode)
                
            }
            
        }
        
        if (contactA == BodyType.Bullet) {
            
            bulletHitEnemy(contact.bodyA.node as! SKSpriteNode, enemy: contact.bodyB.node as! SKSpriteNode)
            
        }
        
        if (contactA == BodyType.Hero) {
            
            heroHitEnemy(contact.bodyA.node as! SKSpriteNode, enemy: contact.bodyB.node as! SKSpriteNode)
            
        }
        
    }

    func bulletHitEnemy(bullet:SKSpriteNode, enemy:SKSpriteNode) {
        bullet.removeFromParent()
        enemy.removeFromParent()
        
        Score++
        scoreLabel.text = "\(Score)"
    }
    

    
    func heroHitEnemy(player:SKSpriteNode, enemy:SKSpriteNode) {
        
    
        removeAllChildren()
        //addChild(redZone)
        var gameOverLabel = SKLabelNode(fontNamed: "Zapfino")
        gameOverLabel.text = "You lose :("
        gameOverLabel.fontColor = UIColor.blueColor()
        gameOverLabel.fontSize = 40
        gameOverLabel.position = CGPointMake(self.size.width/2, self.size.height/2)
        
        addChild(gameOverLabel)
    }
  
}

